import { getIn } from 'formik';
import { Form } from 'react-bootstrap';

export default function InputText (props) {

    const { field, form: { touched, errors }, label, max, min, extraOnChange, ...rest } = props;
    
    return (
        <Form.Group>
            <Form.Label>{label}</Form.Label>
            <Form.Control 
                type="text" 
                maxLength={max ? max : "60"}
                minLength={min ? min : "2"}
                className={
                    `form-control 
                    ${getIn(touched, field.name) && getIn(errors, field.name) && 'is-invalid'}
                `}
                {...field}
                {...rest} 
            >
            </Form.Control>
            <div style={{minHeight:"1.5rem", marginTop: "0.325rem"}} id="error-wrapper">
                {getIn(touched, field.name) && getIn(errors, field.name) && (
                    <span className="invalid-feedback d-block">
                        {getIn(errors, field.name)}
                    </span>
                )}
            </div>
        </Form.Group>
    )
}