import { Field } from "formik"
import { Button, Col, Row } from "react-bootstrap"
import InputText from "../BaseInputs/text"
import { MapContainer } from "./Map"

export const BodyForm = ({props}) => {
    const p = props;
    return (
        <Col>
            <Row>
                <Col>
                    <Field 
                        component={InputText}
                        name="name"
                        placeholder="Nombre completo"
                        label={"Nombre completo"}
                    />
                </Col>
                <Col>
                    <Field 
                        component={InputText}
                        name="phone"
                        placeholder="Ej: 0414-1234567"
                        label={"Telefono"}
                        max={"13"}
                        min={"11"}
                    />
                </Col>
                <Col>
                    <Field 
                        component={InputText}
                        name="idCode"
                        placeholder="Ej: V12.123.456"
                        label={"Documento de identidad"}
                        max={"9"}
                        min={"7"}
                    />
                </Col>
            </Row>
            <Row className="mb-4 text-center">
            
                <Col>
                    <span>
                        Eliga una ubicacion en el mapa
                    </span>
                    <MapContainer props={props} />
                </Col>
            </Row>
            <Row>
                <Col>
                    <Button variant="primary" type="submit" disabled={!props.isValid}>
                        Submit
                    </Button>
                </Col>
            </Row>
        </Col>
    )
}