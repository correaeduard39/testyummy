import { Formik, Form as FForm } from "formik";
import { Container, Jumbotron } from "react-bootstrap";
import { initialValuesForm } from "./initialValues";
import { SchemaForm } from "./formSchema";
import { FormYummy } from "./formYummy";
import clientAxios from "../../config/configAxios";

function MyForm() {

    const handleSubmit = async (values, actions) => {
        try {
            const res = await clientAxios.post('https://recruitment-two.vercel.app/api/v1/recruitment/prod', values);
        } catch (error) {
            console.log(error)
        }
    }
  
    return (
        <Container>
            <Jumbotron>
                <Formik
                    initialValues={initialValuesForm}
                    validationSchema={SchemaForm}
                    validateOnChange={true}
                    onSubmit={(values, actions ) => {
                        handleSubmit(values, actions);
                    }}
                >
                { props => (
                    <FForm>
                        <FormYummy formik={props} />
                    </FForm>
                )}
                </Formik>
            </Jumbotron>
        </Container>
    );
  }
  
  export default MyForm;