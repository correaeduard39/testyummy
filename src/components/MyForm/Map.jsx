import { GoogleMap, useJsApiLoader, Marker  } from '@react-google-maps/api';
import { useCallback, useState } from 'react';

const containerStyle = {
    width: '100%',
    height: '400px'
};
  
const center = {
    lat: 10.48801,
    lng: -66.87919
};

export const MapContainer = ({props}) => {

    const { setFieldValue } = props;
    const { isLoaded } = useJsApiLoader({
        id: 'google-map-script',
        googleMapsApiKey: process.env.REACT_APP_GM_API_KEY
    });

    const [map, setMap] = useState(null);

    const onLoad = useCallback(function callback(map) {
        const bounds = new window.google.maps.LatLngBounds();
        map.fitBounds(bounds);
        setMap(map)
    }, []);

    const onUnmount = useCallback(function callback(map) {
        setMap(null)
    }, [])

    const handleChangePosition = (e) => {
        const mylat = e.latLng.lat();
        const mylng = e.latLng.lng()
        setFieldValue("location", {"lat": mylat.toFixed(5), "long": mylng.toFixed(5) });
    }

    return isLoaded ? (
        <GoogleMap
          mapContainerStyle={containerStyle}
          center={center}
          position={center}
          zoom={10}
          onLoad={onLoad}
          onUnmount={onUnmount}
        >
           <Marker
                clickable={true}
                position={center}
                //position= {GoogleMap.maps.LatLng(center.lat,center.lng)}
                draggable={true}
                onDragEnd={(e) => handleChangePosition(e) }
            />
          <></>
        </GoogleMap>
    ) : <></>
}