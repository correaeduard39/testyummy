import { BodyForm } from "./bodyForm"

export const FormYummy = (props) => {
    const fprops = props.formik;
    return (
        <div className="row mt-3">
            <div className="col-12 col-md-10 mx-auto">
                <div className="card">
                    <div className="card-body">
                        <h3 className="font-size-ubii-16 text-center mt-3">
                            Formulario Yummy
                        </h3>

                        <BodyForm props={fprops}/>
                    </div>
                </div>
            </div>
        </div>
    )

}