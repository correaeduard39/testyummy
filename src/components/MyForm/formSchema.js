import * as Yup from 'yup';
import { regexCI, regexLat, regexLong, regexNameValidator, regexPhoneNumber } from '../../helpers';

export const SchemaForm = Yup.object({
    name: Yup.string()
        .matches(regexNameValidator, '- Nombre no valido -')
        .required('- Campo obligatorio -'),
    location: Yup.object({
        lat: Yup.string()
            .matches(regexLat, '- Nombre no valido -')
            .required('- Campo obligatorio -'),
        long: Yup.string()
            .matches(regexLong, '- Nombre no valido -')
            .required('- Campo obligatorio -'),
    }),
    phone: Yup.string()
        .matches(regexPhoneNumber, '- Numero de teléfono no valido -')
        .required('- Campo obligatorio -'),
    idCode: Yup.string()
        .matches(regexCI, '- Cedula no valida -')
        .required('- Campo obligatorio -')
});
