import axios from "axios";

const clientAxios = axios.create({

    baseURL: process.env.backendURL,
    headers:{
        'Accept' : 'application/json; charset=utf-8',
        'Content-Type' : 'application/json',
        'Access-Control-Allow-Origin': "*"
    }

});

export default clientAxios;